#include <set>
#include <cstdio>
#include <cstdlib>

using namespace std;

struct matrix_entity {
	int x, y;
	double v;

	friend bool operator <(matrix_entity a, matrix_entity b) {
		if (a.x == b.x)
			return a.y < b.y;
		return a.x < b.x;
	}
};

int main(void) {
	set<matrix_entity> entities;

	int row_num = 1e5;
	int col_num = 1e5;
	double density = 0.01;

	int entity_cnt = 0;
	int required_cnt = int(density * row_num * col_num);
	printf("density: %lf, entity num: %d\n", density, required_cnt);

	while (entity_cnt < required_cnt) {
		matrix_entity tmp;
		tmp.x = rand() % row_num;
		tmp.y = rand() % col_num;
		if (entities.find(tmp) == entities.end()) {
			tmp.v = (double) rand() / RAND_MAX;
			entities.insert(tmp);
			entity_cnt++;
		}
	}

	FILE *fp;
	fp = fopen("sparse_coo_matrix_1e5x1e5_01density.txt", "w");
	for (set<matrix_entity>::iterator iter = entities.begin();
			iter != entities.end(); iter++) {
		fprintf(fp, "%d %d %lf\n", iter->x, iter->y, iter->v);
	}
	fclose(fp);
	return 0;
}

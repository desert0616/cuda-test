#include "cusparse.h"
#include "gputimer.h"
#include <cstdio>
#include <cstdlib>

int main(void) {
	FILE *fp;
	fp = fopen("sparse_coo_matrix_1e5x1e5_01density.txt", "r");
	int row_num = 1e5;
	int col_num = 1e5;
	int nnz = 1e8;

	int *h_coo_idx_x = (int*) malloc(nnz * sizeof(int));
	int *h_coo_idx_y = (int*) malloc(nnz * sizeof(int));
	double *h_coo_value = (double*) malloc(nnz * sizeof(double));

	for (int i = 0; i < nnz; i++) {
		fscanf(fp, "%d %d %lf", &h_coo_idx_x[i], &h_coo_idx_y[i],
				&h_coo_value[i]);
	}
	fclose(fp);

	double *h_x = (double*) malloc(row_num * sizeof(double));
	for (int i = 0; i < row_num; i++)
		h_x[i] = (double) rand() / RAND_MAX;
	double *h_y = (double*) malloc(row_num * sizeof(double));

	cusparseHandle_t handle = 0;
	cusparseMatDescr_t descr = 0;

	cusparseCreate(&handle);
	cusparseCreateMatDescr(&descr);
	cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO);

	int* d_coo_idx_x;
	int* d_coo_idx_y;
	double* d_coo_value;
	cudaMalloc(&d_coo_idx_x, nnz * sizeof(int));
	cudaMalloc(&d_coo_idx_y, nnz * sizeof(int));
	cudaMalloc(&d_coo_value, nnz * sizeof(double));
	cudaMemcpy(d_coo_idx_x, h_coo_idx_x, nnz * sizeof(int),
			cudaMemcpyHostToDevice);
	cudaMemcpy(d_coo_idx_y, h_coo_idx_y, nnz * sizeof(int),
			cudaMemcpyHostToDevice);
	cudaMemcpy(d_coo_value, h_coo_value, nnz * sizeof(double),
			cudaMemcpyHostToDevice);

	double* d_x;
	double* d_y;
	cudaMalloc(&d_x, row_num * sizeof(double));
	cudaMalloc(&d_y, row_num * sizeof(double));
	cudaMemcpy(d_x, h_x, row_num * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemset(d_y, 0, row_num * sizeof(double));

	int* d_csr_row_ptr;
	cudaMalloc(&d_csr_row_ptr, (row_num + 1) * sizeof(int));

	GpuTimer timer;
	timer.Start();

	// construct csr
	cusparseXcoo2csr(handle, d_coo_idx_x, nnz, row_num, d_csr_row_ptr,
			CUSPARSE_INDEX_BASE_ZERO);

	// spmv
	double dOne = 1.0;
	cusparseDcsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, row_num, col_num,
			nnz, &dOne, descr, d_coo_value, d_csr_row_ptr, d_coo_idx_y, d_x,
			&dOne, d_y);
	timer.Stop();

	cudaMemcpy(h_y, d_y, row_num * sizeof(double), cudaMemcpyDeviceToHost);

	printf("Time: %g ms.\n", timer.Elapsed());

	return 0;
}
